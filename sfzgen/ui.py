#!/usr/bin/env python3

import functools

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
from gi.repository import Gtk, Gdk

from .offsets import OffsetsManager
from .base import SampleId

class MyWindow(OffsetsManager, Gtk.Window):
    @property
    def offset_step(self):
        return self.period()

    def change_offset(self, move=1, snap=True):
        sid = self.current
        refsid = sid.reference
        if refsid == sid: return
        offset, step = self.offset, self.offset_step
        offset += move * step
        if snap:
            # The domain is split into consecutive ranges of length |step|.
            # |offset| was the best fit in his own range. Snap to the beginning
            # of the range, so that the new found offset will be the best there
            offset -= offset % step
            offset = self.find_offset(refsid, sid, offset, offset + step - 1,
                    len(self.sample) // 2)
        self.offset = offset

    def update(self):
        self.set_title("{!s} [{}] - {:.1f}Hz \"{}\"".format(
            self.current, self.offset,
            self.current.freq, self.current.filename))
        self.queue_draw()

    def on_key(self, _, event):
        if event.keyval == Gdk.KEY_Right:
            if event.state & Gdk.ModifierType.CONTROL_MASK:
                self.offset += -1
            else:
                self.change_offset(-1,
                        not event.state & Gdk.ModifierType.SHIFT_MASK)
        elif event.keyval == Gdk.KEY_Left:
            if event.state & Gdk.ModifierType.CONTROL_MASK:
                self.offset += 1
            else:
                self.change_offset(1,
                        not event.state & Gdk.ModifierType.SHIFT_MASK)
        elif event.keyval == Gdk.KEY_Up:
            self.current = self.current.prev()
        elif event.keyval == Gdk.KEY_Down:
            self.current = self.current.next()
        elif event.keyval == Gdk.KEY_Page_Up:
            self.current = self.current.prev(outer=True)
        elif event.keyval == Gdk.KEY_Page_Down:
            self.current = self.current.next(outer=True)
        elif event.keyval in (Gdk.KEY_Return, Gdk.KEY_KP_Enter):
            self.validate()
        elif event.keyval == Gdk.KEY_p:
            for sid in SampleId.all():
                refsid = sid.reference
                if refsid == sid: continue
                print("Aligning offset for {!s}.".format(sid))
                self.offsets[sid] = self.find_offset(
                        refsid, sid,
                        self.offsets[sid] - self.period(sid) // 3,
                        self.offsets[sid] + self.period(sid) // 3,
                        len(self.sample) // 4)

        self.update()
        return True

    def offsets_changed(self):
        super().offsets_changed()
        self.peak.cache_clear()
        self.update()

    @functools.lru_cache()
    def peak(self, sid, length):
        return max(map(abs, self.realsample(sid, length)))

    def on_draw(self, _, context, *args):
        current = self.current

        a = self.draw.get_allocation()
        width, height = a.width, a.height
        hscroll = int(self.scroll.get_value())
        _, _, key, _ = current
        hmax = width + 5 * self.period()
        vmax = self.peak(current, int(hmax)) * 1.1 # headroom
        def pos(x, y):
            return ((x-hscroll) / hmax * (width-50) + 50,
                    (1 - y / vmax) * height * 0.5)
        def dot(x, y, size=2):
            x, y = pos(x,y)
            context.rectangle(x-size/2, y-size/2, size, size)
        context.set_line_width(0.5)
        context.set_source_rgb(0.2,0.2,0.2)
        context.move_to(0, pos(0,0)[1])
        context.line_to(width, pos(0,0)[1])
        context.stroke()
        context.move_to(*pos(0,-vmax))
        context.line_to(*pos(0,vmax))
        context.stroke()

        context.set_source_rgba(0,0.5,0,0.5)
        for p in range(0, hmax, self.period()):
            x = p + hscroll
            context.move_to(*pos(x, -vmax))
            context.line_to(*pos(x, -vmax/2))
            context.stroke()

        def draw_curve(values, color,
                       xoffset=self.realoffset(current),
                       scale=1):
            context.set_source_rgba(*color)
            for x, y in enumerate(values):
                x, y = pos(x-xoffset,y*scale)
                if x < 0: continue
                context.line_to(x, y)
                if x > width: break
            context.stroke()
        draw_curve(self.sample, (0,0,0,1))
        draw_curve(self.power, (0,0,1,0.5))

        refsid = current.reference
        if current != refsid:
            draw_curve(self.samples[refsid],
                        (1,0,0,0.5),
                        self.realoffset(refsid),
                        scale=self.factor(current, refsid))


    def __init__(self):
        Gtk.Window.__init__(self)
        OffsetsManager.__init__(self)

        self.update()

        self.OK = False

        self.resize(1200, 400)

        self.connect("delete-event", Gtk.main_quit)
        self.connect("key-press-event", self.on_key)

        drawing = self.draw = Gtk.DrawingArea()
        drawing.connect("draw", self.on_draw)

        scroll = self.scroll = Gtk.Scrollbar()
        scroll.set_orientation(Gtk.Orientation.HORIZONTAL)
        scroll.get_adjustment().set_upper(len(self.sample))
        scroll.connect("value-changed", lambda *a: self.queue_draw())


        vbox = Gtk.Box()
        vbox.set_orientation(Gtk.Orientation.VERTICAL)
        vbox.pack_start(drawing, True, True, 0)
        vbox.pack_start(scroll, False, False, 0)

        self.add(vbox)

    def validate(self, *_):
        self.OK = True
        Gtk.main_quit()
