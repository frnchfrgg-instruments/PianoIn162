import itertools, collections, math

import soundfile
import numpy as np

# ------ iterator tools -------
def chunks(iterable, length, slide = 0):
    if slide == 0: slide = length
    if slide == 1:
        iters = list(itertools.tee(iterable, length))
        for skip, it in enumerate(iters):
            next(itertools.islice(it, skip, skip), None)
        return zip(*iters)

    grouped = zip(*([iter(iterable)] * slide))
    if length > slide:
        grouped = map( itertools.chain.from_iterable,
                       chunks(grouped,
                              (length - 1) // slide + 1,
                              slide=1) )
    if length % slide == 0:
        return grouped
    return ( tuple(itertools.islice(it, length)) for it in grouped )

# ------ math tools ------

def average(array):
    return np.sum(array) / len(array)

def moving_average(array, window=128):
    ret = np.cumsum(array)
    ret[window:] = ret[window:] - ret[:-window]
    return ret[window - 1:] / window

def meanprod(array1, array2):
    try:
        return average(array1 * array2)
    except IndexError:
        return 0

def moving_meanprod(array1, array2, window=128):
    return moving_average(array1 * array2, window)

def rms(array):
    return math.sqrt(meanprod(array, array))

def moving_rms(array, window=128):
    return np.sqrt(moving_meanprod(array, array, window))

# ------ audio tools ------

AudioInfo = collections.namedtuple('AudioInfo',
        'samplerate channels')

def getchannels(sample, count=None):
    with soundfile.SoundFile(sample) as A:
        info = getchannels.lastinfo = AudioInfo(
                samplerate = A.samplerate,
                channels   = A.channels)
        if count is None:
            count = A.frames
        elif count < 0:
            count = int(-count * info.samplerate)
        count = min(A.frames, count)
        interleaved = A.read(frames=count, dtype='float32', always_2d=True)
        channels = interleaved.T # transpose
        return channels

def kmeter(channel, window=128):
    z1 = z2 = 0
    omega = 2 / window
    it = iter(channel)
    for s in itertools.islice(it, window // 2):
        z1 += omega * (s*s - z1)
        z2 += omega * (z1 - z2)
    for s in channel:
        z1 += omega * (s*s - z1)
        z2 += omega * (z1 - z2)
        yield math.sqrt(2*z2)

def find_trim(channel, silence_factor=0.1, window=128, maxtrim=-1):
    power = kmeter(channel, window)
    if maxtrim < 0:
        power = list(power)
    else:
        power = list(itertools.islice(power, maxtrim))
    thr = max(power) * silence_factor
    return next( x for x,p in enumerate(power) if p >= thr )


# adaptation of audioop.findfit algorithm
# minpos is included, maxpos excluded, as range() does
FitResult = collections.namedtuple('FitResult', 'pos fit factor')

def bestfit(channel, fragment, minpos=0, maxpos=None):
    minpos = max(0, min(minpos, len(channel) - 1))
    if minpos + len(fragment) > len(channel):
        fragment = fragment[:len(channel)-minpos]
    window = len(fragment)
    if maxpos is None:
        maxpos = len(channel)
    else:
        maxpos = max(minpos, min(maxpos + window, len(channel)))
    channel = channel[minpos:maxpos]

    frag_ms = meanprod(fragment, fragment) # sum_ri_2
    channel_mms = moving_meanprod(channel, channel, window) # sum_aij_2

    fit, pos, factor = float("inf"), 0, 0
    for j, chanms in enumerate(channel_mms):
        cross = meanprod(channel[j:j+window], fragment) # sum_aij_ri
        fact = cross / frag_ms
        if fact < 0: continue
        curfit = frag_ms - cross**2 / chanms
        if curfit < fit:
            fit, pos, factor = curfit, j, fact

    return FitResult(pos=pos+minpos, fit=fit, factor=factor)

def findfactor(reference, channel):
    try:
        return meanprod(reference, channel) / meanprod(channel, channel)
    except ZeroDivisionError:
        return 1
