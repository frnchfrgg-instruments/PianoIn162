__all__ = ["OffsetsManager"]

import itertools, functools, collections
from . import audiotools

import os
import json

from .base import *

class SampleStore():
    @functools.lru_cache(len(list(Pedals)) * len(list(Velocities)) * 2 + 1)
    def __getitem__(self, sid):
        return audiotools.getchannels(sid.filename, -4)[0]

class SignalingDict(dict):
    def __init__(self, changed=None):
        super().__init__()
        self.changed = changed
    def __setitem__(self, sid, value):
        super().__setitem__(sid, value)
        if callable(self.changed): self.changed()
    def update(self, *a, **kw):
        super().update(*a, **kw)
        if callable(self.changed): self.changed()

class OffsetsManager():
    to_clear = ()

    @property
    def maxlen(self):
        return len(self.sample) // 10
    @property
    def offset_range(self):
        return len(self.sample) // 5

    def period(self, sid=None):
        if sid is None: sid = self.current
        return int(audiotools.getchannels.lastinfo.samplerate / sid.freq)

    def __init__(self, statefile="saved_state", backup=None):
        if backup is None: backup = statefile + ".old"
        self.state_file = statefile
        self.backup_file = backup

        self.samples = SampleStore() # container with LRU cache

        # load last saved state
        state = None
        try:
            with open(self.state_file, "r") as f: state = json.load(f)
            # backup the state
            os.replace(self.state_file, self.backup_file)
        except:
            pass
        if type(state) is not dict:
            state = {}

        if "current" in state:
            self.current = SampleId.from_str(state["current"])
        else:
            self.current = SampleId.first()

        self.offsets = SignalingDict(self.offsets_changed)
        self.offsets.update( (SampleId.from_str(s), o)
                             for s, o in state.get("offsets", {}).items() )

        # compute all missing offsets with a heuristic
        for sid in SampleId.all():
            if sid in self.offsets: continue
            print("Searching offset for {!s}.".format(sid))
            refsid = sid.reference
            if refsid == sid:
                self.offsets[sid] = self.find_trim(sid, 0.1)
            else:
                estimated = self.find_trim(sid, 0.3) - \
                        self.find_trim(refsid, 0.3)
                estimated = self.find_offset(
                        refsid, sid,
                        estimated - self.offset_range // 2,
                        estimated + self.offset_range // 2,
                        self.maxlen // 10)
                self.offsets[sid] = self.find_offset(
                        refsid, sid,
                        estimated - self.period(sid) // 2,
                        estimated + self.period(sid) // 2,
                        self.maxlen)

    def save(self):
        # save our state
        state = {}
        state["current"] = str(self.current)
        O = self.offsets
        state["offsets"] = collections.OrderedDict(
                (str(sid), O[sid]) for sid in SampleId.all()
                if sid in O)
        with open(self.state_file, "w") as f:
            json.dump(state, f, indent=4)

    @functools.lru_cache()
    def _realoffsets(self, first):
        offsets = {}
        for sid in first.siblings():
            offsets[sid] = self.offsets.get(sid, 0)
            refsid = sid.reference # assumes the reference is before
            if refsid != sid: offsets[sid] += offsets[refsid]
        mini = min(offsets.values())
        for sid in first.siblings(): offsets[sid] -= mini
        return offsets
    def realoffset(self, sid):
        return self._realoffsets(sid.first_sibling())[sid]
    to_clear +=  (_realoffsets.cache_clear,)

    @functools.lru_cache()
    def factor(self, refsid, sid=None, maxlen=None):
        maxlen = maxlen or self.maxlen
        if sid is None:
            return self.factor(refsid.reference, refsid) # try the cache
        ref = self.realsample(refsid, maxlen)
        frag = self.realsample(sid, maxlen)
        return audiotools.findfactor(ref, frag)
    to_clear += (factor.cache_clear,)

    @functools.lru_cache()
    def _gains(self, first):
        sid = first.last_sibling()
        gains = { sid: 1 / audiotools.rms(self.realsample(sid)) }
        while True:
            refsid = sid.reference
            if refsid == sid: break
            gains[refsid] = gains[sid] * self.factor(sid, refsid)
            sid = refsid
        for sid in first.siblings():
            if sid in gains: continue
            refsid = sid.reference
            gains[sid] = gains[refsid] * self.factor(refsid, sid)
        return gains
    def gain(self, sid):
        return self._gains(sid.first_sibling())[sid]
    to_clear += (_gains.cache_clear,)

    @functools.lru_cache()
    def find_trim(self, sid, fact):
        return audiotools.find_trim(self.samples[sid], fact,
                                    maxtrim=self.maxlen)

    def find_offset(self, refsid, sid, minoffset, maxoffset, fraglen=None):
        if fraglen is None: fraglen = self.maxlen
        trim = max(self.find_trim(sid, 0.5), maxoffset)
        fragment = self.samples[sid][trim:trim+fraglen]
        fit = audiotools.bestfit(self.samples[refsid], fragment,
                                 minpos=trim - maxoffset,
                                 maxpos=trim - minoffset)
        return trim - fit.pos

    @property
    def current(self):
        return self._current
    @current.setter
    def current(self, val):
        old = getattr(self, "_current", None)
        self._current = val

        self.rms = audiotools.rms(self.sample)
        self.power = list(audiotools.kmeter(self.sample))
        self.maxpower = max(self.power, default=0)

    @property
    def sample(self):
        return self.samples[self.current]
    @property
    def offset(self):
        return self.offsets[self.current]
    @offset.setter
    def offset(self, value):
        self.offsets[self.current] = value

    def realsample(self, sid, length = None):
        sample = self.samples[sid]
        offset = self.realoffset(sid)
        if length is None: length = len(sample) // 2
        return sample[offset:offset+length]

    def aux(to_clear):
        def offsets_changed(self):
            for f in to_clear: f()
            self.save()
        return offsets_changed
    offsets_changed = aux(to_clear)
    del to_clear
