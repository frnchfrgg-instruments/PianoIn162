__all__ = ["Velocities", "Pedals", "RoundRobin", "Keys", "SampleId"]


import enum
import itertools, operator, collections

class CyclicEnum(enum.Enum):
    def __init__(self, *_):
        real_members = list(type(self))
        if real_members:
            self._prev = real_members[-1]
            self._prev._next = self
            self._next = real_members[0]
            self._next._prev = self
        else:
            self._prev = self._next = self

    def next(self):
        return self._next

    def prev(self):
        return self._prev

    @classmethod
    def first(cls):
        return next(iter(cls))

    @classmethod
    def last(cls):
        return cls.first().prev()

A = enum.auto
class Velocities(CyclicEnum):
    Pianissimo = A()
    Piano      = A()
    MezzoPiano = A()
    MezzoForte = A()
    Forte      = A()

class Pedals(CyclicEnum):
    Off = A()
    On  = A()

class RoundRobin(CyclicEnum):
    RR1 = A()
    RR2 = A()

class KeysMeta(type):
    def __iter__(cls):
        for i in range(cls._count_): yield cls(i)
class Keys(int, metaclass=KeysMeta):
    __slots__ = ()
    _count_ = 88

    def next(self):
        return type(self)((self + 1) % self._count_)

    def prev(self):
        return type(self)((self - 1) % self._count_)

    @property
    def name(self):
        return str(self)

    @classmethod
    def first(cls):
        return cls()

    @classmethod
    def last(cls):
        return cls(cls._count_-1)


class SampleId(collections.namedtuple("SampleId", "rr pedal key vel")):
    __slots__ = ()

    @property
    def filename(self):
        return "Piano in 162 Samples/Close/" \
                "Pedal{pedal}Close/" \
                "{keyid:0>2d}-Pedal{pedal}{vel}{rr}Close.flac".format(
                    rr = self.rr.name[2], pedal = self.pedal.name,
                    keyid = self.key+1, vel = self.vel.name)

    def next(self, outer=False):
        if outer:
            key = self.key.next()
            rr = self.rr.next() if key == Keys.first() else self.rr
            return self._replace(key = key, rr = rr)
        else:
            pedal = self.pedal.next()
            vel = self.vel.next() if pedal == Pedals.first() else self.vel
            return self._replace(pedal = pedal, vel = vel)

    def prev(self, outer=False):
        if outer:
            key = self.key.prev()
            rr = self.rr.prev() if key == Keys.last() else self.rr
            return self._replace(key = key, rr = rr)
        else:
            pedal = self.pedal.prev()
            vel = self.vel.prev() if pedal == Pedals.last() else self.vel
            return self._replace(pedal = pedal, vel = vel)

    def outer(self):
        return (self.rr, self.key)

    def inner(self):
        return (self.vel, self.pedal)

    def siblings(self):
        for V, P in itertools.product(Velocities, Pedals):
            yield self._replace(pedal = P, vel = V)

    def first_sibling(self):
        return next(self.siblings())

    def last_sibling(self):
        return self.first_sibling().prev(outer=False)

    @property
    def reference(self):
        if self.first_sibling() == self:
            return self
        elif self.pedal == Pedals.first():
            # align pedal offs together
            return self._replace(vel = self.vel.prev())
        else:
            # align pedal on with pedal off
            return self._replace(pedal = Pedals.first())

    def __str__(self):
        return " ".join(map(operator.attrgetter("name"), iter(self)))

    @property
    def freq(self):
        return 2 ** ((self.key + 21 - 69)/12) * 440

    @property
    def period(self):
        return 1 / self.freq

    @classmethod
    def from_str(cls, string):
        try:
            rr, pedal, key, vel = string.split()
        except (AttributeError, ValueError):
            raise ValueError from None
        try:
            rr = next(x for x in RoundRobin if x.name == rr)
            pedal = next(x for x in Pedals if x.name == pedal)
            vel = next(x for x in Velocities if x.name == vel)
        except StopIteration:
            raise ValueError from None
        try:
            key = Keys(key)
        except ValueError:
            raise ValueError from None
        return cls(rr, pedal, key, vel)


    @classmethod
    def first(cls):
        return cls(RoundRobin.first(), Pedals.Off.first(),
                   Keys.first(), Velocities.first())

    @classmethod
    def last(cls):
        return cls(RoundRobin.last(), Pedals.Off.last(),
                   Keys.last(), Velocities.last())

    @classmethod
    def all(cls):
        return ( cls(rr, ped, key, vel)
                 for rr, key, vel, ped
                 in itertools.product(RoundRobin, Keys, Velocities, Pedals) )
