import math
import itertools, functools

from .base import *
from . import audiotools

def to_dB(gain):
    try:
        return 20 * math.log10(gain)
    except ValueError:
        return -120.0

def from_dB(dB):
    return 10 ** (dB / 20)



def velocity_select(low, high, xfade):
    low = -xfade if low is None else low - xfade // 2
    high = 127 if high is None else high - xfade // 2
    return ("lovel={lo} hivel={hi}\n"
           "xfin_lovel={inlo} xfin_hivel={inhi}\n"
           "xfout_lovel={outlo} xfout_hivel={outhi}\n"
           "xf_velcurve=gain\n".format(
                   lo=max(0,low), hi=min(high+xfade,127),
                   inlo=max(0,low), inhi=low+xfade,
                   outlo=high, outhi=min(high+xfade,127)
                ))

def generate(manager, global_volume=-60, xfade=16,
        basename="PianoIn162"):
    def region(f, sid, opcodes = ''):
        # no dampers for last notes
        loop_mode = "" if sid.key < 68 else " loop_mode=one_shot"
        volume = global_volume + to_dB(manager.gain(sid))
        # the note region
        print("Adding region for {}".format(sid))
        f.write("<region>{} key={} sample={} "
                "offset={} volume={:.2f}{}\n".format(
                    loop_mode, sid.key+21, sid.filename,
                    manager.realoffset(sid), volume,
                    ' ' + opcodes if opcodes else ''
                ))

    maxvel = {
            Velocities.Pianissimo: 41,
            Velocities.Piano: 61,
            Velocities.MezzoPiano: 81,
            Velocities.MezzoForte: 101,
            Velocities.Forte: None
        }
    def velgroup(f, sid, opcodes=''):
        lowvel, highvel = maxvel[sid.vel.prev()], maxvel[sid.vel]
        roundrobin = sid.rr.value
        pedal = sid.pedal.value
        opcodes = ("{opc}{vel}".format(
                        opc = opcodes,
                        vel = velocity_select(lowvel, highvel, xfade)))
        f.write("<group>\n{}\n".format(opcodes))
        for key in Keys:
            region(f, sid._replace(key = key))
        f.write("\n")

    pedalopcode = {
            Pedals.Off: "amplitude_curvecc64=7",
            Pedals.On:  "amplitude_curvecc64=8"
        }
    rropcode = {
            RoundRobin.RR1: "hirand=0.5",
            RoundRobin.RR2: "lorand=0.5"
        }
    def group(f, sid, opcodes=''):
        roundrobin = rropcode[sid.rr]
        pedal = pedalopcode[sid.pedal]
        opcodes = f""
        f.write("<master>\n")
        f.write(f"{opcodes}{roundrobin}\n{pedal}\n\n")
        notes = f.name + "-Notes"
        f.write(f'#include "{notes}"\n')
        with open(notes, "w") as f2:
            for vel in Velocities:
                velgroup(f2, sid._replace(vel = vel))

    def curves(f):
        def volcurve(val):
            distance = min(1, abs(val - 63.5) / 30)
            ratio = (1 - (1 - distance) ** 2.0) * (-1 if val < 63.5 else 1)
            return (ratio + 1) / 2
        f.write("<curve>\n")
        for i in range(0,128):
            val = 1 - volcurve(i)
            f.write(f"v{i}={val:.3f}\n")
        f.write("\n<curve>\n")
        for i in range(0,128):
            val = volcurve(i)
            f.write(f"v{i}={val:.3f}\n")

    with open(basename + ".sfz", "w") as main:
        def subsfz(role):
            filename = "{}-{}".format(basename, role)
            main.write('#include "{}"\n'.format(filename))
            return open(filename, "w")
        with subsfz("Curves") as f:
            curves(f)
        opcodes = ("xf_cccurve=gain\n"
                   "ampeg_attack=.005\n"
                   "ampeg_release=0.95\n"
                   "amplitude_oncc64=100 amplitude_smoothcc64=350\n"
                   )
        main.write("<global>\n{}\n".format(opcodes))
        for rr, ped in itertools.product(RoundRobin, Pedals):
            with subsfz("Pedal{}-{}".format(ped.name, rr.name)) as f:
                group(f, SampleId(rr = rr, pedal = ped,
                                  vel = Velocities.first(),
                                  key = Keys.first()))



"""
def velocity_to_gain(vel):
    return (vel**2 / 127**2)# * 0.77 + (vel / 127) * 0.20 + 0.03

for n in ("Release-Hammer", "Release-Strings"):
    m.write('#include "PianoIn162-{}"\n'.format(n))
m.close()
"""
